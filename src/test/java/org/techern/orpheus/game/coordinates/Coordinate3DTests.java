package org.techern.orpheus.game.coordinates;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A class that tests {@link IntCoordinate3D} and, in the future, FloatCoordinate3D
 *
 * @since 0.0.1
 */
public class Coordinate3DTests {

    /**
     * A coordinate at 0,0,0
     *
     * @since 0.0.1
     */
    private IntCoordinate3D intCoordinateZero;

    /**
     * A coordinate at 1000,1000,1000
     *
     * @since 0.0.1
     */
    private IntCoordinate3D intCoordinateThousand;

    /**
     * Populates the {@link IntCoordinate3D}s used in tests
     *
     * @since 0.0.1
     */
    @Before
    public void populateCoordinates() {
        intCoordinateZero = new IntCoordinate3D(0, 0, 0);
        intCoordinateThousand = new IntCoordinate3D(1000, 1000, 1000);
    }

    /**
     * Tests cloning of the coordinates
     *
     * @since 0.0.1
     */
    @Test
    public void testClone() {
        IntCoordinate3D zero = intCoordinateZero.clone();

        IntCoordinate3D thousand = intCoordinateThousand.clone();

        Assert.assertEquals(zero.getX(), intCoordinateZero.getX());
        Assert.assertEquals(zero.getY(), intCoordinateZero.getY());
        Assert.assertEquals(zero.getZ(), intCoordinateZero.getZ());

        Assert.assertEquals(thousand.getX(), intCoordinateThousand.getX());
        Assert.assertEquals(thousand.getY(), intCoordinateThousand.getY());
        Assert.assertEquals(thousand.getZ(), intCoordinateThousand.getZ());

    }

    /**
     * Tests comparison against a {@link IntCoordinate2D}
     *
     * @since 0.0.1
     */
    @Test
    public void testAgainst2D() {
        IntCoordinate2D zero2 = new IntCoordinate2D(intCoordinateZero.getX(), intCoordinateZero.getY());

        //noinspection AssertEqualsBetweenInconvertibleTypes
        Assert.assertEquals(intCoordinateZero, zero2);
        Assert.assertNotEquals(intCoordinateThousand, zero2);
    }

    /**
     * Tests cloning of the coordinates
     *
     * @since 0.0.1
     */
    @Test
    public void testCloneEquals() {
        IntCoordinate3D zero = intCoordinateZero.clone();

        IntCoordinate3D thousand = intCoordinateThousand.clone();

        Assert.assertEquals(zero, intCoordinateZero);
        Assert.assertEquals(thousand, intCoordinateThousand);

        Assert.assertNotEquals(zero, thousand);
        Assert.assertNotEquals(zero, intCoordinateThousand);

        Assert.assertNotEquals(thousand, intCoordinateZero);

        Assert.assertTrue(thousand.equals(intCoordinateThousand));

    }
}
