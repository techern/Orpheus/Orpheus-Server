package org.techern.orpheus.game.coordinates;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A class that tests {@link IntCoordinate2D} and, in the future, FloatCoordinate2D
 *
 * @since 0.0.1
 */
public class Coordinate2DTests {

    /**
     * A coordinate at 0,0
     *
     * @since 0.0.1
     */
    private IntCoordinate2D intCoordinateZero;

    /**
     * A coordinate at 1000,1000
     *
     * @since 0.0.1
     */
    private IntCoordinate2D intCoordinateThousand;

    /**
     * Populates the {@link IntCoordinate2D}s used in tests
     *
     * @since 0.0.1
     */
    @Before
    public void populateCoordinates() {
        intCoordinateZero = new IntCoordinate2D(0, 0);
        intCoordinateThousand = new IntCoordinate2D(1000, 1000);
    }

    /**
     * Tests cloning of the coordinates
     *
     * @since 0.0.1
     */
    @Test
    public void testClone() {
        IntCoordinate2D zero = intCoordinateZero.clone();

        IntCoordinate2D thousand = intCoordinateThousand.clone();

        Assert.assertEquals(zero.getX(), intCoordinateZero.getX());
        Assert.assertEquals(zero.getY(), intCoordinateZero.getY());

        Assert.assertEquals(thousand.getX(), intCoordinateThousand.getX());
        Assert.assertEquals(thousand.getY(), intCoordinateThousand.getY());

    }

    /**
     * Tests comparison against a {@link IntCoordinate3D}
     *
     * @since 0.0.1
     */
    @Test
    public void testAgainst3D() {
        IntCoordinate3D zero3 = new IntCoordinate3D(intCoordinateZero, 0);

        //noinspection AssertEqualsBetweenInconvertibleTypes
        Assert.assertEquals(intCoordinateZero, zero3);
        Assert.assertNotEquals(intCoordinateThousand, zero3);
    }

    /**
     * Tests cloning of the coordinates
     *
     * @since 0.0.1
     */
    @Test
    public void testCloneEquals() {
        IntCoordinate2D zero = intCoordinateZero.clone();

        IntCoordinate2D thousand = intCoordinateThousand.clone();

        Assert.assertEquals(zero, intCoordinateZero);
        Assert.assertEquals(thousand, intCoordinateThousand);

        Assert.assertNotEquals(zero, thousand);
        Assert.assertNotEquals(zero, intCoordinateThousand);

        Assert.assertNotEquals(thousand, intCoordinateZero);

        Assert.assertTrue(thousand.equals(intCoordinateThousand));

    }
}
