package org.techern.orpheus;

import org.techern.orpheus.cache.CacheManager;
import org.techern.orpheus.logging.LogManager;
import org.techern.orpheus.networking.OrpheusNetworkHandler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * The main class for {@link OrpheusServer}
 *
 * TODO: Strip everything out of here by 0.1 and only have the entry point
 *
 * @since 0.0.2
 */
public class OrpheusServer {

    /**
     * The {@link LocalDateTime} at which time the {@link OrpheusServer} started
     *
     * @since 0.0.1
     */
    public static LocalDateTime SERVER_START_TIME = LocalDateTime.now();

    /**
     * The {@link CacheManager} for the server
     *
     * @since 0.0.1
     */
    public static CacheManager CACHE_MANAGER = new CacheManager();

    /**
     * The connection port
     *
     * TODO: Move this into configuration
     * @since 0.0.1
     */
    public static int CONNECTION_PORT = 43594;

    /**
     * The {@link LogManager} instance for {@link OrpheusServer}
     *
     * @since 0.0.1
     */
    public static LogManager ORPHEUS_LOG_MANAGER = LogManager.getManager("OrpheusServer");

    /**
     * The main entry point
     *
     * @param arguments The commandline arguments
     * @since 0.0.1
     */
    public static void main(String... arguments) {

        ORPHEUS_LOG_MANAGER.logTrace("Orpheus startup called");

        ORPHEUS_LOG_MANAGER.logInfo("Server starting at " + SERVER_START_TIME.toString());

        CACHE_MANAGER.initialize();

        ORPHEUS_LOG_MANAGER.logDebug("Starting network manager");

        try {
            new OrpheusNetworkHandler().run();
        } catch (Exception exception) {
            if (exception.getMessage().contains("Address already in use")) {
                ORPHEUS_LOG_MANAGER.logCriticalError("You're already running a server on this port. Please kill it and try again");
                System.exit(1);
            } else {
                exception.printStackTrace();
                ORPHEUS_LOG_MANAGER.logCriticalError("Critical error in network stack: " + exception + "... Please write a bug report at https://gitlab.com/techern/Orpheus-Server/issues");
            }
        }

        ORPHEUS_LOG_MANAGER.logCriticalError("Well, shit, the main and/or network thread died.");
    }
}
