package org.techern.orpheus.game.coordinates;

/**
 * An interface depicting a basic set of coordinates
 *
 * @since 0.0.1
 */
public interface ICoordinate<T> extends Cloneable {

    /**
     * Gets the X coordinate
     *
     * @return The X coordinate
     */
    public T getX();

    /**
     * Gets the Y coordinate
     *
     * @return The Y coordinate
     */
    public T getY();
}
