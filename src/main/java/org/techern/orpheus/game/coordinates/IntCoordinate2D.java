package org.techern.orpheus.game.coordinates;

import org.techern.orpheus.logging.LogManager;

/**
 * An implementation of {@link ICoordinate} that represents a point in 2-dimensional space
 *
 * @since 0.0.1
 */
public class IntCoordinate2D implements ICoordinate<Integer> {

    /**
     * Creates a new {@link IntCoordinate2D}
     *
     * @param x The X coordinate
     * @param y The Y coordinate
     *
     * @since 0.0.1
     */
    public IntCoordinate2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * The X coordinate
     *
     * @since 0.0.1
     */
    private int x;

    /**
     * The Y coordinate
     *
     * @since 0.0.1
     */
    private int y;

    /**
     * Gets the X coordinate
     *
     * @return The X coordinate
     *
     * @since 0.0.1
     */
    @Override
    public Integer getX() {
        return x;
    }

    /**
     * Gets the Y coordinate
     *
     * @return The Y coordinate
     *
     * @since 0.0.1
     */
    @Override
    public Integer getY() {
        return y;
    }

    /**
     * Clones this {@link IntCoordinate2D}
     *
     * @return A new {@link IntCoordinate2D} with the same data
     * @since 0.0.1
     */
    public IntCoordinate2D clone() {
        return new IntCoordinate2D(x, y);
    }

    @Override
    public int hashCode() {
        return (this.x * 18) + (this.y * 42);
    }

    @Override
    public boolean equals(Object other) {

        if (IntCoordinate3D.class.isAssignableFrom(other.getClass())) {
            IntCoordinate3D coordinate = (IntCoordinate3D) other;
            return (coordinate.getX() == this.x) && (coordinate.getY() == this.y);
        } else if (IntCoordinate2D.class.isAssignableFrom(other.getClass())) {
            IntCoordinate2D coordinate = (IntCoordinate2D) other;
            return (coordinate.getX() == this.x) && (coordinate.getY() == this.y);
        } else {
            LogManager.getManager("IntCoordinate2D").logWarning("Attempt to compare an IntCoordinate2D to a " + other.getClass().getName());
            return false;
        }

    }
}
