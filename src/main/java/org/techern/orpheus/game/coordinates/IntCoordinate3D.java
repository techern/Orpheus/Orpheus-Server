package org.techern.orpheus.game.coordinates;

import org.techern.orpheus.logging.LogManager;

/**
 * An implementation of {@link ICoordinate} that represents a point in 2-dimensional space
 *
 * @since 0.0.1
 */
public class IntCoordinate3D implements ICoordinate<Integer> {

    /**
     * Creates a new {@link IntCoordinate3D}
     *
     * @param x The X coordinate
     * @param y The Y coordinate
     * @param z The Z coordinate
     *
     * @since 0.0.1
     */
    public IntCoordinate3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Creates a new {@link IntCoordinate3D}
     *
     * @param flatCoordinate The {@link IntCoordinate2D} representing the flat plane
     * @param z The Z coordinate
     *
     * @since 0.0.1
     */
    public IntCoordinate3D(IntCoordinate2D flatCoordinate, int z) {
        this.x = flatCoordinate.getX();
        this.y = flatCoordinate.getY();
        this.z = z;
    }

    /**
     * The X coordinate
     *
     * @since 0.0.1
     */
    private int x;

    /**
     * The Y coordinate
     *
     * @since 0.0.1
     */
    private int y;

    /**
     * The Z coordinate
     *
     * @since 0.0.1
     */
    private int z;

    /**
     * Gets the X coordinate
     *
     * @return The X coordinate
     *
     * @since 0.0.1
     */
    @Override
    public Integer getX() {
        return x;
    }

    /**
     * Gets the Y coordinate
     *
     * @return The Y coordinate
     *
     * @since 0.0.1
     */
    @Override
    public Integer getY() {
        return y;
    }

    /**
     * Gets the Z coordinate
     *
     * @return The Z coordinate
     *
     * @since 0.0.1
     */
    public Integer getZ() {
        return z;
    }

    /**
     * Clones this {@link IntCoordinate3D}
     *
     * @return A new {@link IntCoordinate3D} with the same data
     * @since 0.0.1
     */
    public IntCoordinate3D clone() {
        return new IntCoordinate3D(x, y, z);
    }

    @Override
    public int hashCode() {
        return (this.x * 18) + (this.y * 42) + (this.z ^ 6);
    }

    @Override
    public boolean equals(Object other) {

        if (IntCoordinate3D.class.isAssignableFrom(other.getClass())) {
            IntCoordinate3D coordinate = (IntCoordinate3D) other;
            return (coordinate.getX() == this.x) && (coordinate.getY() == this.y) && (coordinate.getZ() == this.z);
        } else if (IntCoordinate2D.class.isAssignableFrom(other.getClass())) {
            IntCoordinate2D coordinate = (IntCoordinate2D) other;
            return (coordinate.getX() == this.x) && (coordinate.getY() == this.y);
        } else {
            LogManager.getManager("IntCoordinate3D").logWarning("Attempt to compare an IntCoordinate3D to a " + other.getClass().getName());
            return false;
        }

    }


}
