package org.techern.orpheus.networking.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.techern.orpheus.networking.packets.IncomingPacket;

/**
 * Handles all {@link IncomingPacket}s
 *
 * @since 0.0.1
 */
public class IncomingPacketHandler extends SimpleChannelInboundHandler<IncomingPacket> {

    /**
     * <strong>Please keep in mind that this method will be renamed to
     * {@code messageReceived(ChannelHandlerContext, I)} in 5.0.</strong>
     * <p>
     *
     * @param ctx the {@link ChannelHandlerContext} which this {@link SimpleChannelInboundHandler}
     *            belongs to
     * @param msg the message to handle
     * @throws Exception is thrown if an error occurred
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket msg) throws Exception {

        msg.process(ctx);

    }
}
