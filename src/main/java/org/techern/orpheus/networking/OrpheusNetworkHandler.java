package org.techern.orpheus.networking;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.AttributeKey;
import org.techern.orpheus.OrpheusServer;
import org.techern.orpheus.networking.decoders.OrpheusConnectionDecoder;

/**
 * The main network handler for {@link OrpheusServer}
 *
 * @since 0.0.1
 */
public class OrpheusNetworkHandler {

    /**
     * The {@link AttributeKey} for each {@link io.netty.channel.Channel}'s status
     *
     * @since 0.0.1
     */
    public final static AttributeKey<ChannelStatus> STATUS_KEY = AttributeKey.valueOf("status");

    /**
     * Runs this {@link OrpheusNetworkHandler}
     *
     * @throws Exception SOMETHING WENT HORRIBLY WRONG!
     * @since 0.0.1
     */
    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap(); // (2)
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) // (3)
                    .childHandler(new ChannelInitializer<SocketChannel>() { // (4)
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("decoder", new OrpheusConnectionDecoder());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)          // (5)
                    .childOption(ChannelOption.SO_KEEPALIVE, true); // (6)

            // Bind and start to accept incoming connections.
            ChannelFuture f = bootstrap.bind(OrpheusServer.CONNECTION_PORT).sync(); // (7)

            // Wait until the server socket is closed.
            // In this example, this does not happen, but you can do that to gracefully
            // shut down your server.
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }


    }


}
