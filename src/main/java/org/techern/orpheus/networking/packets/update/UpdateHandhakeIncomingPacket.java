package org.techern.orpheus.networking.packets.update;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.techern.orpheus.logging.LogManager;
import org.techern.orpheus.networking.packets.IncomingPacket;

public class UpdateHandhakeIncomingPacket extends IncomingPacket {
    /**
     * Gets the required number of bytes to decode this packet, or -1 or -2 TODO Research which negative value represents what
     *
     * @return The required number of bytes in the {@link ByteBuf}
     * @since 0.0.1
     */
    @Override
    public int getRequiredSize() {
        return 6;
    }

    /**
     * Gets the ID of this {@link IncomingPacket}
     *
     * @return The ID
     * @since 0.0.1
     */
    @Override
    public int getID() {
        return 15;
    }

    /**
     * The client's version
     *
     * @since 0.0.1
     */
    private int clientVersion;

    /**
     * The client's language
     *
     * @since 0.0.1
     */
    private int clientLanguage;

    /**
     * Decodes this {@link IncomingPacket}
     *
     * @param context The {@link ChannelHandlerContext} context
     * @param buffer  The {@link ByteBuf} containing byte-data
     * @since 0.0.1
     */
    @Override
    public void decode(ChannelHandlerContext context, ByteBuf buffer) {
        this.clientVersion = buffer.readInt();
        buffer.readByte(); //TODO Update this to something worth reading
        this.clientLanguage = buffer.readByte();
    }

    /**
     * Processes this {@link IncomingPacket}
     *
     * @param context The {@link ChannelHandlerContext} context in case we need to drop connection or send a packet
     * @since 0.0.1
     */
    @Override
    public void process(ChannelHandlerContext context) {
        //Ok... Now this will one day be updated
        //But for now we send a headless byte 0
        context.writeAndFlush(context.alloc().buffer(1).writeByte(0));
    }
}
