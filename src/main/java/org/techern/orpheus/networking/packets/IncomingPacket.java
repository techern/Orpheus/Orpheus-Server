package org.techern.orpheus.networking.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * An abstract class defining an incoming packet
 *
 * @since 0.0.1
 */
public abstract class IncomingPacket {

    /**
     * Gets the required number of bytes to decode this packet, or -1 or -2 TODO Research which negative value represents what
     *
     * @return The required number of bytes in the {@link ByteBuf}
     * @since 0.0.1
     */
    public abstract int getRequiredSize();

    /**
     * Gets the ID of this {@link IncomingPacket}
     *
     * @return The ID
     * @since 0.0.1
     */
    public abstract int getID();

    /**
     * Decodes this {@link IncomingPacket}
     *
     * @param context The {@link ChannelHandlerContext} context
     * @param buffer The {@link ByteBuf} containing byte-data
     *
     * @since 0.0.1
     */
    public abstract void decode(ChannelHandlerContext context, ByteBuf buffer);

    /**
     * Processes this {@link IncomingPacket}
     *
     * @param context The {@link ChannelHandlerContext} context in case we need to drop connection or send a packet
     *
     * @since 0.0.1
     */
    public abstract void process(ChannelHandlerContext context);

}
