package org.techern.orpheus.networking;

/**
 * An enum depicting the connection type
 *
 * @since 0.0.1
 */
public enum ConnectionType {

    /**
     * We don't know this connection. May not be a client! Treat with caution
     *
     * @since 0.0.1
     */
    UNKNOWN,

    /**
     * A web client is trying to connect... Ok
     *
     * @since 0.0.1
     */
    HTTP,

    /**
     * The client is requesting a cache update or check
     *
     * @since 0.0.1
     */
    UPDATE_SERVER,

    /**
     * The client thinks we are only a lobby server. Gotta love this shit, hey?
     *
     * @since 0.0.1
     */
    LOBBY,

    /**
     * The client thinks we're the game server! Celebrations inbound!
     *
     * @since 0.0.1
     */
    GAME_SERVER

}
