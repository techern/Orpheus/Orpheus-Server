package org.techern.orpheus.networking;

/**
 * An enumeration depicting the status of a {@link io.netty.channel.Channel}
 *
 * @since 0.0.1
 *
 * TODO: LOGIN? ACCOUNT_REGISTRATION? Should we handle HTTP traffic and display server status?
 */
public enum ChannelStatus {

    /**
     * Channel state is unknown, we'll just dump everything
     *
     * @since 0.0.1
     */
    UNKNOWN,

    /**
     * Currently setting up a connection
     *
     * @since 0.0.1
     */
    HANDSHAKE,

    /**
     * Requesting an update, usually (always?) the cache
     *
     * @since 0.0.1
     */
    UPDATE,

    /**
     * We are in the lobby
     *
     * @since 0.0.1
     */
    LOBBY,

    /**
     * We are now in the game
     *
     * @since 0.0.1
     */
    GAME;

}
