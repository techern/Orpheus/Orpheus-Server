package org.techern.orpheus.networking.decoders;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.techern.orpheus.logging.LogManager;
import org.techern.orpheus.networking.ChannelStatus;
import org.techern.orpheus.networking.OrpheusNetworkHandler;
import org.techern.orpheus.networking.handlers.IncomingPacketHandler;
import org.techern.orpheus.networking.packets.update.UpdateHandhakeIncomingPacket;
;

import java.util.List;

/**
 * A {@link io.netty.handler.codec.ByteToMessageDecoder} for previously unknown connections
 *
 * @since 0.0.1
 */
public class OrpheusConnectionDecoder extends ByteToMessageDecoder {

    /**
     * The {@link LogManager} used in {@link OrpheusConnectionDecoder}
     *
     * @since 0.0.1
     */
    LogManager LOGGER = LogManager.getManager("OrpheusConnectionDecoder");

    /**
     * Decode the from one {@link ByteBuf} to an other. This method will be called till either the input
     * {@link ByteBuf} has nothing to read when return from this method or till nothing was read from the input
     * {@link ByteBuf}.
     *
     * @param ctx the {@link ChannelHandlerContext} which this {@link ByteToMessageDecoder} belongs to
     * @param in  the {@link ByteBuf} from which to read data
     * @param out the {@link List} to which decoded messages should be added
     * @throws Exception is thrown if an error occurs
     */
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        if (ctx.channel().hasAttr(OrpheusNetworkHandler.STATUS_KEY)) {

            switch (ctx.channel().attr(OrpheusNetworkHandler.STATUS_KEY).get()) {

                case UNKNOWN:
                    LOGGER.logDebug("Unknown connection from " + ctx.channel().remoteAddress() + " tried to talk again. Disconnecting it! AGAIN!");
                    ctx.channel().disconnect();
                    break;

                case UPDATE:
                    decodeUpdate(ctx, in, out);
                    break;

                default:
                    LOGGER.logWarning("Unknown connection: " + in.readByte());

            }
        } else {
            decodeNewConnection(ctx, in, out);
        }

    }

    private void decodeNewConnection(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        byte connectionType = in.getByte(0);

        LOGGER.logInfo("New connection from " + ctx.channel().remoteAddress() + ", type " + connectionType);

        switch (connectionType) {
            case 15: //TODO Change this to something custom :)
                if (in.readableBytes() >= 6) { //TODO Update this when we change it :p
                    ctx.channel().attr(OrpheusNetworkHandler.STATUS_KEY).set(ChannelStatus.UPDATE);
                    in.readByte(); //Discard type

                    UpdateHandhakeIncomingPacket packet = new UpdateHandhakeIncomingPacket();

                    packet.decode(ctx, in);

                    out.add(packet);

                    ctx.pipeline().addLast("handler", new IncomingPacketHandler());

                } else {
                    //Not enough data in frame. We return
                    return;
                }
                break;

            default:
                ctx.channel().attr(OrpheusNetworkHandler.STATUS_KEY).set(ChannelStatus.UNKNOWN);
                ctx.disconnect();
                LOGGER.logInfo("Unknown connection! Closing before it can even communicate.");
                break;

        }

        //Well let's have a go at decoding again LOL
        //decode(ctx, in, out); WAIT NO

    }

    private void decodeUpdate(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() == 0) { //Whenever the channel is inactive decode will be called - This is the default behavior inherited from ByteToMessageDecoder
            return;
        } else {
            LOGGER.logInfo(in.readableBytes() + "???");
            //...
        }

    }

}
