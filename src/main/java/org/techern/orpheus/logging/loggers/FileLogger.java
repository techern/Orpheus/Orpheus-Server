package org.techern.orpheus.logging.loggers;

import org.techern.orpheus.OrpheusServer;
import org.techern.orpheus.logging.LogManager;
import org.techern.orpheus.logging.LogType;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class FileLogger extends Logger {

    /**
     * The file lock
     *
     * @since 0.0.1
     */
    private final Object lock = new Object();

    /**
     * The {@link Path} to the file
     *
     * @since 0.0.1
     */
    private Path filePath;

    /**
     * Creates a new {@link FileLogger}
     *
     * @param manager The {@link LogManager} managing this logger
     * @since 0.0.1
     */
    public FileLogger(LogManager manager) {
        super(manager);

        //Ok, first check to see if the directories are available

        Path logFilePath = Paths.get("logs", String.valueOf(OrpheusServer.SERVER_START_TIME.getYear()),
                String.valueOf(OrpheusServer.SERVER_START_TIME.getMonth()),
                String.valueOf(OrpheusServer.SERVER_START_TIME.getDayOfMonth()),
                OrpheusServer.SERVER_START_TIME.getHour() + "-" + OrpheusServer.SERVER_START_TIME.getMinute() + "-" + OrpheusServer.SERVER_START_TIME.getSecond());

        synchronized (lock) {
            try {
                if (!Files.exists(logFilePath)) {
                    Files.createDirectories(logFilePath);
                }
                logFilePath = logFilePath.resolve(manager.getName() + ".log");
                if (!Files.exists(logFilePath)) {
                    filePath = Files.createFile(logFilePath);
                } else {
                    filePath = logFilePath;
                }

            } catch (IOException e) {
                //This is the only time we'll fucking leave
                System.err.println("Failed to create File Logger " + manager.getName());
                e.printStackTrace();
            }
        }

    }

    /**
     * Logs a {@link String} message
     *
     * @param message The message to log
     * @param type    The {@link LogType} of the message. No need to validate, it's already been done in the {@link LogManager}
     * @since 0.0.1
     */
    @Override
    public void log(String message, LogType type) {

        try {
            BufferedWriter writer = Files.newBufferedWriter(filePath, Charset.forName("UTF-8"), StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            writer.write(LocalDateTime.now().toString() + " | " + getManager().getName() + " - " +
                    "[" + type.name() + "] - " + message + "\n");
            writer.close();
        } catch (Exception e) {
            System.out.println("Error while logging! Logger is " + getManager().getName());
            e.printStackTrace();
        }

    }
}
