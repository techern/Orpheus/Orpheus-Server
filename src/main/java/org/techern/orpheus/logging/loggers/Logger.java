package org.techern.orpheus.logging.loggers;

import org.techern.orpheus.logging.LogManager;
import org.techern.orpheus.logging.LogType;

/**
 * An abstract {@link Logger}
 *
 * @since 0.0.1
 */
public abstract class Logger {

    /**
     * The {@link LogManager} managing this {@link Logger}
     *
     * @since 0.0.1
     */
    private LogManager manager;

    /**
     * Gets the {@link LogManager} managing this {@link Logger}
     *
     * @return The {@link LogManager}
     * @since 0.0.1
     */
    public LogManager getManager() {
        return manager;
    }

    /**
     * Creates a new {@link Logger}
     *
     * @param manager The {@link LogManager} managing this logger
     * @since 0.0.1
     */
    public Logger(LogManager manager) {
        this.manager = manager;
    }

    /**
     * Logs a {@link String} message
     *
     * @param message The message to log
     * @param type The {@link LogType} of the message. No need to validate, it's already been done in the {@link LogManager}
     *
     * @since 0.0.1
     */
    public abstract void log(String message, LogType type);

}
