package org.techern.orpheus.logging.loggers;

import org.techern.orpheus.OrpheusServer;
import org.techern.orpheus.logging.LogManager;
import org.techern.orpheus.logging.LogType;

import java.time.LocalDateTime;

/**
 * A {@link Logger} that simply logs to the console
 *
 * @since 0.0.1
 */
public class ConsoleLogger extends Logger {


    /**
     * Creates a new {@link ConsoleLogger}
     *
     * @param manager The {@link LogManager} managing this logger
     * @since 0.0.1
     */
    public ConsoleLogger(LogManager manager) {
        super(manager);
    }

    /**
     * Logs a {@link String} message
     *
     * @param message The message to log
     * @param type    The {@link LogType} of the message. No need to validate, it's already been done in the {@link LogManager}
     * @since 0.0.1
     */
    @Override
    public void log(String message, LogType type) {

        System.out.println(LocalDateTime.now().toString() + " | " + getManager().getName() + " - " +
                "[" + type.name() + "] - " + message);
    }
}
