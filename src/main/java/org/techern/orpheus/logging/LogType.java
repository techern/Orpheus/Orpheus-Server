package org.techern.orpheus.logging;

/**
 * An enumeration describing the severity of a single log entry
 *
 * @since 0.0.1
 */
public enum LogType {

    /**
     * A critical error - Usually non-recoverable. The application will crash or already has
     *
     * @since 0.0.1
     */
    CRITICAL_ERROR((byte) 30),

    /**
     * An error - Something happened that should never happen. It may be recoverable but once these start appearing, you're in trouble!
     *
     * @since 0.0.1
     */
    ERROR((byte) 20),

    /**
     * A warning. Something should not have happened, but maybe it's possible? Investigate when possible
     *
     * @since 0.0.1
     */
    WARNING((byte) 15),

    /**
     * Information - The equivalent of regular printing to the console
     *
     * @since 0.0.1
     */
    INFORMATION((byte) 10),

    /**
     * Debugging information that normally isn't printed, but when enabled, should be useful in finding an issue
     *
     * @since 0.0.1
     */
    DEBUG((byte) 5),

    /**
     * Miscellaneous information that would usually be discarded. In the event that DEBUG is useless, it may prove handy...
     *
     * Or you'll suddenly get a 2GB log file. Who knows?
     *
     * @since 0.0.1
     */
    TRACE((byte) 1);

    /**
     * Creates a new {@link LogType}
     *
     * @param id The numeric ID
     *
     * @since 0.0.1
     */
    LogType(byte id) {
        this.id = id;
    }

    /**
     * The integer ID of this {@link LogType}
     *
     * @since 0.0.1
     */
    private byte id; //Currently set as a byte to shave that tiny bit of memory

    /**
     * Gets this {@link LogType}'s ID
     *
     * @return The ID
     * @since 0.0.1
     */
    public byte getID() {
        return id;
    }

}
