package org.techern.orpheus.logging;

import org.techern.orpheus.logging.loggers.ConsoleLogger;
import org.techern.orpheus.logging.loggers.FileLogger;
import org.techern.orpheus.logging.loggers.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A manager for various logging functions
 *
 * @since 0.0.1
 */
public class LogManager {;

    /**
     * The limit on the {@link LogType}s we will log. Set to INFORMATION by default
     *
     * @since 0.0.1
     */
    public static LogType LOG_TYPE_LIMIT = LogType.INFORMATION;

    /**
     * The name of this {@link LogManager}
     *
     * It will not be possible to change this once it is set, since some loggers require the name to remain the same
     *
     * @since 0.0.1
     */
    private String name;

    /**
     * The {@link Logger}s managed by this {@link LogManager}
     *
     * @since 0.0.1
     */
    private List<Logger> loggers = new ArrayList<>();

    /**
     * A {@link HashMap} of {@link LogManager}s
     *
     * @since 0.0.1
     */
    private static Map<String, LogManager> managerMap = new HashMap<>(2);

    /**
     * Creates a new {@link LogManager}
     *
     * @param name The name of this {@link LogManager}
     *
     * @throws IllegalArgumentException It already exists
     * @since 0.0.1
     */
    private LogManager(String name) throws IllegalArgumentException {
        this.name = name;

        this.supplyDefaultLoggers();

    }

    /**
     * Supplies the default {@link org.techern.orpheus.logging.loggers.Logger}s
     *
     * @since 0.0.1
     */
    private void supplyDefaultLoggers() {
        loggers.add(new ConsoleLogger(this));
        loggers.add(new FileLogger(this));
    }

    /**
     * Gets the {@link LogManager} for the requested name
     *
     * @param managerName The manager's name
     * @return A {@link LogManager}
     *
     * @since 0.0.1
     */
    public static LogManager getManager(String managerName) {
        if (managerMap.containsKey(managerName)) {
            return managerMap.get(managerName);
        } else {
            LogManager manager = new LogManager(managerName);

            managerMap.put(managerName, manager);

            return getManager(managerName);
        }
    }

    /**
     * Gets the name of this {@link LogManager}
     *
     * @return The name
     * @since 0.0.1
     */
    public String getName() {
        return name;
    }

    /**
     * Logs a {@link String} message to all {@link Logger}s registered
     *
     * @param message The message
     * @param type The {@link LogType} associated with this log
     *
     * @since 0.0.1
     */
    public void log(String message, LogType type) {
        //Check to see if it's a loggable message
        if (type.getID() >= LOG_TYPE_LIMIT.getID()) {
            for (Logger logger : loggers) {
                logger.log(message, type);
            }
        }
        //If we've reached here, we just discard the message
    }

    /**
     * Logs a critical error
     *
     * @param message The message
     * @since 0.0.1
     */
    public void logCriticalError(String message) {
        log(message, LogType.CRITICAL_ERROR);
    }

    /**
     * Logs an error
     *
     * @param message The message
     * @since 0.0.1
     */
    public void logError(String message) {
        log(message, LogType.ERROR);
    }

    /**
     * Logs a warning
     *
     * @param message The message
     * @since 0.0.1
     */
    public void logWarning(String message) {
        log(message, LogType.WARNING);
    }

    /**
     * Logs an information message
     *
     * @param message The message
     * @since 0.0.1
     */
    public void logInfo(String message) {
        log(message, LogType.INFORMATION);
    }

    /**
     * Logs a debug message
     *
     * @param message The message
     * @since 0.0.1
     */
    public void logDebug(String message) {
        log(message, LogType.DEBUG);
    }

    /**
     * Logs a trace message
     *
     * @param message The message
     * @since 0.0.1
     */
    public void logTrace(String message) {
        log(message, LogType.TRACE);
    }


}
