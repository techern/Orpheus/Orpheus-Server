package org.techern.orpheus.cache;

import org.techern.orpheus.logging.LogManager;

/**
 * Manages the cache
 *
 * @since 0.0.1
 */
public class CacheManager {

    /**
     * A {@link LogManager} for {@link CacheManager}
     *
     * @since 0.0.1
     */
    private static LogManager LOGGER = LogManager.getManager("CacheManager");

    /**
     * Initializes the {@link CacheManager}
     *
     * @since 0.0.1
     */
    public void initialize() {

        LOGGER.logInfo("Cache initialization started");

        //TODO Actually do things

        LOGGER.logInfo("Cache initialization finished");

    }
}
